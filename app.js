class Employee {
    constructor(options){
        this.name = options.name
        this.age = options.age
        this.salary = options.salary
    }
    get employeeName(){
return this.name
    }
set employeeName(newName){
    this.name = typeof newName === 'string' ?  newName : `invalid name`
    
}
get employeeAge(){
    return this.age
}
set employeeAge(newAge){
this.age = typeof newAge === 'number' && newAge > 16 ? newAge : `invalid age`
}
get employeeSalary(){
    return this.salary
}
set employeeSalary(newSalary){
this.salary = typeof newSalary === 'number' ? newSalary : `didn't get salary`
}

}

const employee = new Employee({

})

employee.employeeName = `John`
employee.employeeAge = 17
employee.employeeSalary = 1000
console.log(employee);

class Programmer extends Employee{
    constructor(options){
        super(options)
        this.lang = options.lang
    }
    get employeeSalary() {
        return this.salary * 3;
    }

    set employeeSalary(newSalary) {
        this.salary = typeof newSalary === 'number' ? newSalary : `didn't get salary`;
    }
}

const juniorProgrammer = new Programmer({
    lang: 'JavaScript',
    age: 20,
    name: 'John'
})

juniorProgrammer.employeeSalary = 700
console.log(juniorProgrammer);
console.log(juniorProgrammer.employeeSalary);

const middleProgrammer = new Programmer({
    lang: 'Java',
    salary: 1200
})

middleProgrammer.employeeAge  = 25
middleProgrammer.employeeName = `Jake`
console.log(middleProgrammer)
const seniorProgrammer = new Programmer({
    lang: 'C++'
})
seniorProgrammer.employeeAge = 28
seniorProgrammer.employeeName = `Denis`
seniorProgrammer.employeeSalary = 2000
console.log(seniorProgrammer);